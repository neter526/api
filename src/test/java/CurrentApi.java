import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
public class CurrentApi {

    String API_KEY = "dcd44c396fd9496193109569ed31ae14";

    @BeforeAll
    public static void setup() {
        RestAssured.baseURI = "https://api.weatherbit.io/v2.0";
    }

    @Test
    public void getRequest() {
        Response response = given().queryParam("lat", "40.730610")
                .queryParam("lon", "-73.935242")
                .queryParam("key", API_KEY)
                .contentType(ContentType.JSON)
                .when()
                .get("/current")
                .then()
                .extract().response();

        Assertions.assertEquals(200, response.statusCode());
        System.out.println(" +++ Data Value +++" + response.prettyPrint());
        System.out.println("state code is = " +response.jsonPath().getString("data[0].state_code"));
            }
}
